# Instruções para executar o pipeline de treino

## Em ambiente de desenvolvimento
Executar o seguinte comando no terminal a depender do ambiente:
`curl -g -d '{"env":"dev-preditivo-train", "model":"nome_do_modelo"}' 'localhost:8000'`
`curl -g -d '{"env":"qa-preditivo-train", "model":"nome_do_modelo"}' 'localhost:8000'`
`curl -g -d '{"env":"prod", "model":"nome_do_modelo"}' 'localhost:8000'`

## Em ambiente de produção
Executar o seguinte comando no terminal:
`curl -g -d '{"model":"nome_do_modelo", "ref_date":"YYYY-MM-DD", "id_empresa":"123"}' 'https://preditivo-predict-zm3msiaf7q-ue.a.run.app'`

---

# Instruções para clonar este repositório 

*Este repositório foi criado como repositório público, mas quaisquer modificações nos branches principais devem ser aprovadas via [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html).*

---

Para clonar, basta abrir o terminal e executar o seguinte comando:
```
git clone git@10.205.17.53:victor.valente/preditivos_predict.git
```

Este passo também pode ser feito em interfaces gráficas integradas ao git. [Mais infos aqui](https://git-scm.com/downloads/guis).

---

# Instruções para contribuir neste projeto

1. **Nunca fazer modificações diretamente no branch `master`.
Todo novo branch deve ser criado a partir do branch `dev`.**
2. Este projeto tem dois branches principais:
    1. Branch `master`, onde vive o código que roda no ambiente de produção.
    2. Branch `dev`, onde vive o código que está na fase de testes.
3. Nomear novos branches de acordo com a id da task respectiva no Zoho. Ex: "I1288"
    - Cada task deve ser desenvolvida em um branch dedicado.
    - Quando a task estiver pronta para ser testada, o responsável pela task deve fazer um merge request no branch `dev`.
4. Os merges entre o branch `dev` e o branch `master` serão feitos periodicamente pelo líder do projeto.
5. O código deve ser autodocumentado por meio das docstrings em padrão Google. [Mais infos aqui](https://github.com/google/styleguide/blob/gh-pages/pyguide.md#38-comments-and-docstrings).
