# Set working directory to be the parent directory of that in which the script is located.
cd "$(dirname "$(dirname "$(readlink -f "$0")")")"

# Authenticate.
gcloud auth activate-service-account machine-learning-qa@gesto-qa.iam.gserviceaccount.com \
--key-file /home/alcides-araujo/deploy_qa/preditivo_train/src/config/machine-learning-qa-gesto-qa-dc1e6de3a282.json

# Submit build.
gcloud builds submit \
--tag gcr.io/gesto-qa/dev-preditivo-train \
--account machine-learning-qa@gesto-qa.iam.gserviceaccount.com \
--project gesto-qa

# Deploy and run container.
gcloud run deploy \
dev-preditivo-train \
--image gcr.io/gesto-qa/dev-preditivo-train \
--allow-unauthenticated \
--platform managed \
--region us-east1 \
--account machine-learning-qa@gesto-qa.iam.gserviceaccount.com \
--project gesto-qa \
--memory 1Gi \
--timeout 10m