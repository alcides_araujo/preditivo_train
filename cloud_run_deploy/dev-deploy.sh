# Set working directory to be the parent directory of that in which the script is located.
cd "$(dirname "$(dirname "$(readlink -f "$0")")")"

# Authenticate.
gcloud auth activate-service-account preditivo-dev@laboratorio-eng-dados.iam.gserviceaccount.com \
--key-file src/config/gcloud_credentials_dev.json

# Submit build.
gcloud builds submit \
--tag gcr.io/laboratorio-eng-dados/dev-preditivo-train \
--account preditivo-dev@laboratorio-eng-dados.iam.gserviceaccount.com \
--project laboratorio-eng-dados

# Deploy and run container.
gcloud run deploy \
dev-preditivo-train \
--image gcr.io/laboratorio-eng-dados/dev-preditivo-train \
--allow-unauthenticated \
--platform managed \
--region us-east1 \
--account preditivo-dev@laboratorio-eng-dados.iam.gserviceaccount.com \
--project laboratorio-eng-dados \
--memory 1Gi \
--timeout 10m