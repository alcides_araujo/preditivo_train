# Set working directory to be the parent directory of that in which the script is located.
cd "$(dirname "$(dirname "$(readlink -f "$0")")")"

# Authenticate.
gcloud auth activate-service-account machine-learning-prd@cosmic-shift-235317.iam.gserviceaccount.com \
--key-file src/config/gcloud_credentials_prod.json

# Submit build.
gcloud builds submit \
--tag gcr.io/cosmic-shift-235317/prd-preditivo-train \
--account machine-learning-prd@cosmic-shift-235317.iam.gserviceaccount.com \
--project cosmic-shift-235317

# Deploy and run container.
gcloud run deploy \
prd-preditivo-train \
--image gcr.io/cosmic-shift-235317/prd-preditivo-train \
--allow-unauthenticated \
--platform managed \
--region us-east1 \
--account machine-learning-prd@cosmic-shift-235317.iam.gserviceaccount.com \
--project cosmic-shift-235317 \
--memory 1Gi \
--timeout 10m \
--cpu 1