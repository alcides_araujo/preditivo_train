from src.etl.ETL import ETL
from src.data_prep.Preprocessing import Preprocessing
from src.training.TrainingPipeline import TrainingPipeline

model_names = ['bariatrica', 'neonatal', 'ortopedia', 'bucomaxilo', 'cardio']


def run_etl(model_name):
    print('Iniciando ETL dos dados de treino.')
    etl = ETL(model_name)
    etl_data = etl.run()
    return etl_data


def run_preprocessing(df):
    print('Iniciando pré-processamento dos dados de treino.')
    prep = Preprocessing()
    preprocessed_df = prep.run(df)
    return preprocessed_df


def run_training(df, model_name):
    print('Iniciando pipeline de treino do modelo.')
    tp = TrainingPipeline(model_name)
    tp.run(df)


def run(request_json):
    """Point of access for the training pipeline.

    Args:
        request_json (dict): Dictionary object which contains the training parameters such as:
            model and ref_date.
    """

    # Get training parameters from request_json data.
    print(f'These are the training pipeline parameters: {request_json}.')

    try:
        model_name = request_json['model']
        if model_name not in model_names:
            raise ValueError(f'Incorrect model name. Try one of these: {model_names}.')
    except KeyError:
        raise KeyError(f'Call to this function must include model name. Try one of these: {model_names}.')

    # ------------------------------------------------------------------------------------------------------#

    print(f'Start training procedure for the following model: {model_name}.')
    etl_data = run_etl(model_name)
    
    preprocessed_df = run_preprocessing(etl_data)

    run_training(preprocessed_df, model_name)
