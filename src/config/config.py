import os

CONFIG_DIR = os.path.dirname(os.path.abspath(__file__))
SRC_DIR = os.path.dirname(CONFIG_DIR)
ROOT_DIR = os.path.dirname(SRC_DIR)

# Runtime environment.
RUNTIME_ENVIRONMENT = os.getenv('RUNTIME_ENVIRONMENT')

# Credenciais de acesso ao GCP.
BIGQUERY_DATASET_NAME = 'fenix_bi'

if RUNTIME_ENVIRONMENT == 'dev':
    GCLOUD_PROJECT = 'laboratorio-eng-dados'
    GCLOUD_BUCKET = 'dev-ml-model'
    GCLOUD_CREDENTIALS_FILEPATH = f'{CONFIG_DIR}/gcloud_credentials_dev.json'
    ETL_INPUT_TABLE = f"{GCLOUD_PROJECT}.{BIGQUERY_DATASET_NAME}.view_fato_sinistro"
elif RUNTIME_ENVIRONMENT == 'qa':
    GCLOUD_PROJECT = 'gesto-qa'
    #### por enquanto, depois apagar ####
    GCLOUD_PROJECT2 = 'laboratorio-eng-dados'
    GCLOUD_CREDENTIALS_FILEPATH2 = f'{CONFIG_DIR}/gcloud_credentials_dev.json'
    #####################################
    GCLOUD_BUCKET = 'qa-ml-model'
    GCLOUD_CREDENTIALS_FILEPATH = f'{CONFIG_DIR}/machine-learning-qa-gesto-qa-dc1e6de3a282.json'
    ETL_INPUT_TABLE = f"{GCLOUD_PROJECT2}.{BIGQUERY_DATASET_NAME}.view_fato_sinistro"
else:
    GCLOUD_PROJECT = 'cosmic-shift-235317'
    GCLOUD_BUCKET = 'prd-ml-models'
    GCLOUD_CREDENTIALS_FILEPATH = f'{CONFIG_DIR}/gcloud_credentials_prod.json'
    ETL_INPUT_TABLE = f"{GCLOUD_PROJECT}.{BIGQUERY_DATASET_NAME}.view_fato_sinistro"

PREDICT_RESULTS_TABLE = f"{GCLOUD_PROJECT}.{BIGQUERY_DATASET_NAME}.fato_preditivo_new"

# Variáveis relacionadas ao GCP Storage.
PREDITIVOS_FOLDER_NAME = 'preditivos'
MODEL_NAME = 'model_struct.joblib'

# Caminho para a pasta de dados de treino do GETL.
GETL_DATA_DIR = f'{ROOT_DIR}/getl_training_data'
GETL_DATA_FILEPATH = f'{GETL_DATA_DIR}/getl_training_data.hdf'

# Caminhos para as pastas de artefatos.
ARTIFACT_DIR = f'{ROOT_DIR}/artifacts'
ETL_DATA_DIR = f'{ARTIFACT_DIR}/etl_data'
MODELS_DIR = f'{ARTIFACT_DIR}/models'
FEATURES_DIR = f'{ARTIFACT_DIR}/features'
METRICS_DIR = f'{ARTIFACT_DIR}/metrics'
METRICS_LOG_DIR = f'{METRICS_DIR}/logs'
METRICS_CM_DIR = f'{METRICS_DIR}/confusion_matrices'
METRICS_MONITORING_DIR = f'{METRICS_DIR}/monitoring'
